package Problem2;

import java.io.Serializable;
import java.util.ArrayList;

public class User implements Serializable {
	private String firstName;
	private String lastName;
	private String userName;
	private ArrayList<Task> tasks;

	public User(String firstName, String lastName, String userName) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.userName = userName;
		tasks = new ArrayList<>();
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public ArrayList<Task> getTasks() {
		return tasks;
	}

	public void setTasks(ArrayList<Task> tasks) {
		this.tasks = tasks;
	}

	/**
	 * This function adds a new task to the list of tasks.
	 *
	 * @param title The title of the task.
	 * @param description The description of the task.
	 */
	public void addTask(String title, String description) {
		this.tasks.add(new Task(title, description));
	}

	/**
	 * This function prints out the title and description of each task in the tasks array.
	 */
	public void showTasks() {
		for (Task task : this.tasks) {
			System.out.println(task.getTitle() + ", " + task.getDescription());
		}
	}
}
