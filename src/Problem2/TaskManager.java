package Problem2;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;

public class TaskManager implements Serializable {

	private ArrayList<User> users;
	private static final int cut = 4;

	public TaskManager() {
		users = new ArrayList<>();
	}

	/**
	 * It reads the task manager from a file, if it exists, and then performs the requested operation
	 */
	public static void main(String[] args) {
		TaskManager taskManager;

		if (Files.exists(Path.of("taskManager.ser"))) {
			taskManager = readUsers();
			if (taskManager == null)
				return;
		} else {
			taskManager = new TaskManager();
		}

		// A switch statement that is checking the first argument passed in to the program.
		switch (args[0]) {
			case "-createUser":
				taskManager.createUser(args[1].substring(cut), args[2].substring(cut), args[3].substring(cut));
				break;
			case "-showAllUsers":
				taskManager.showAllUsers();
				break;
			case "-addTask":
				taskManager.addTask(args[1].substring(cut), args[2].substring(cut), args[3].substring(cut));
				break;
			case "-showTasks":
				taskManager.showTasks(args[1].substring(cut));
				break;
			default:
				return;
		}

		serializeUsers(taskManager);
	}

	/**
	 * Serialize the TaskManager object to a file called taskManager.ser.
	 *
	 * @param taskManager The TaskManager object that contains all the users and tasks.
	 */
	private static void serializeUsers(TaskManager taskManager) {
		try {
			ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("taskManager.ser"));
			out.writeObject(taskManager);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * > Reads the serialized TaskManager object from the file "taskManager.ser" and returns it
	 *
	 * @return The TaskManager object is being returned.
	 */
	private static TaskManager readUsers() {
		TaskManager taskManager = null;
		try {
			ObjectInputStream in = new ObjectInputStream(new FileInputStream("taskManager.ser"));
			taskManager = (TaskManager) in.readObject();
			in.close();
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}

		return taskManager;
	}

	/**
	 * Show the tasks of the user with the given username.
	 *
	 * @param userName The name of the user whose tasks you want to see.
	 */
	private void showTasks(String userName) {
		User user = this.findByUserName(userName);
		if (user == null)
			return;
		user.showTasks();
	}

	/**
	 * Add a task to the user's list of tasks.
	 *
	 * @param userName The username of the user to add the task to.
	 * @param title The title of the task
	 * @param description The description of the task.
	 */
	private void addTask(String userName, String title, String description) {
		User user = this.findByUserName(userName);
		if (user == null)
			return;

		user.addTask(title, description);
	}

	/**
	 * Find a user by their username.
	 *
	 * @param userName The username of the user to be found.
	 * @return A User object.
	 */
	private User findByUserName(String userName) {
		for (User user : this.users) {
			if (user.getUserName().equals(userName))
				return user;
		}

		return null;
	}

	/**
	 * For each user in the users list, print the user's first name, last name, and the number of tasks the user has.
	 */
	private void showAllUsers() {
		for (User user : this.users) {
			System.out.println(user.getFirstName() + " " + user.getLastName() + ", " + user.getTasks().size());
		}
	}

	/**
	 * This function creates a new user and adds it to the list of users.
	 *
	 * @param firstName The first name of the user.
	 * @param lastName The last name of the user.
	 * @param userName The user's username.
	 */
	private void createUser(String firstName, String lastName, String userName) {
		this.addUser(new User(firstName, lastName, userName));
	}

	/**
	 * If the user is not already in the list, add it.
	 *
	 * @param user The user to add to the list.
	 */
	private void addUser(User user) {
		if (this.contains(user.getUserName()))
			return;
		this.users.add(user);
	}

	/**
	 * If the username is already in the list, return true, otherwise return false.
	 *
	 * @param userName The username of the user to be added.
	 * @return A boolean value.
	 */
	private boolean contains(String userName) {
		for (User user : users) {
			if (user.getUserName().equalsIgnoreCase(userName))
				return true;
		}
		return false;
	}
}
