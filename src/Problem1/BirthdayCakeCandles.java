package Problem1;

import java.util.Arrays;
import java.util.Scanner;

public class BirthdayCakeCandles {
	private int n;
	private int[] candles;

	/**
	 * It takes an array of integers, and returns the number of times the largest integer appears in the array
	 */
	public static void main(String[] args) {
		System.out.println(new BirthdayCakeCandles().run());
	}

	/**
	 * We read the input, find the maximum value, and then return the number of times that value appears in the array
	 *
	 * @return The number of candles that can be blown out.
	 */
	private int run() {
		readInput();
		int max = Arrays.stream(candles).max().getAsInt();

		return  Arrays.stream(candles)
								.filter(i -> i == max)
								.toArray().length;
	}

	/**
	 * It reads the input from the console and stores it in the variables `n` and `candles`
	 */
	private void readInput() {
		Scanner scanner = new Scanner(System.in);

		n = scanner.nextInt();
		candles = new int[n];
		for (int i = 0; i < n; i++) {
			candles[i] = scanner.nextInt();
		}
	}
}
