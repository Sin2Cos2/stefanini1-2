# Stefanini Java Internship
## Rusu Dionisie
___
### Exercise 1

We read from stdin n - number of candles and list of numbers which represents height of candle[i]
Using a stream, we find max value in array. Using this value, we can filter array, to obtain new array
which will contain only candles with height max, and return the length of array.

___
### Exercise 2
This part was implemented using serialization. Second variant, with database is in another repository.
I check if I already have a file with serialized taskManager. If i do, then initialize class from file, otherwise create
new object. Second step is to determinate functionality program have to execute. This part was implemented with a switch.

- createUser - Check if user with given username already is in array. Create new user and add in array if isn't
- showAllUsers - Iterate through array and print information about each user
- addTask - Check if user with given username is in array. Add new task in array of tasks, if user was found.
- showTasks - Check if user with given username is in array. If user was found, iterate through user's array and print information about each task

